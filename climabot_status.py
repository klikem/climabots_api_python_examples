import requests

host = 'test'
url = 'http://api.climabots.com/rabbit/' + host + '/climabot'

token = '0650e9cc26d59d7d1e0ef4e6af0e933d'

climabot_status = {
	"id":"2465747",
	"type":"controlpro",
	"data":[{  
		"Socket1":"1",
		"Socket2":"0",
		"Socket3":"0",
		"Socket4":"0"
	}],
	"ipaddress":"192.168.0.82",
	"battery":"N/A",
	"diagnostics":"External System"
}

#use the 'headers' parameter to set the HTTP headers:
response = requests.post(url, json = climabot_status, headers = {"Authorization": token})

if response.ok:
	print('saved')
else: 
	print(response)

#the 'demopage.asp' prints all HTTP Headers